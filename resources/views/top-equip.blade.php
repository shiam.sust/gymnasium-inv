

<div class=" small_screen_font pt-3" style="min-height: 320px">
    <div style="cursor: pointer">
        <table style="width: 100%; color: white">
            <tr>
                <td colspan="2">
                    <div style="font-size: 18px; ">
                        <input style="display: inline; margin-right:10px; margin-bottom:10px" id="eqp" class="checkbox" type="checkbox" name="equipments[]" value="{{$eq->id}}">{{$eq->name}}
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; border-bottom: 0px !important">
                    <div class="">
                        <div>
                            <div style="font-size: 16px; margin-bottom: 8px"><b>{{$eq->model_id}}</b></div>
                            <div class="text-left">L*W*H (mm)</div>
                            <div class="text-right" style="font-size: 14px">{{$eq->length}} * {{$eq->width}} * {{$eq->height}} </div>
                            <div class="text-left mt-1 mb-1" style="font-size: 14px">Net Weight: &nbsp;&nbsp;{{$eq->weight}} kg</div>
                            <div class="text-left" style="font-size: 14px">希望小売価格:</div>
                            <div class="text-right" style="font-size: 25px">¥ {{$eq->price_main}}</div>
                            <div class="text-left" style="font-size: 14px">税込:</div>
                            <div class="text-right" style="font-size: 25px">¥ {!!floor($eq->price_main * (110/100))!!}</div>
                        </div>
                    </div>
                </td>
                <td style="width: 50%; padding: 0px; border: none; padding-top: 40px">
                    <div style="height:200px; width:auto; background-color:#fff; background-image: url({{url('assets/equips/'.$eq->image)}});background-repeat: no-repeat;
                                background-position: center center; background-size: cover;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    {{--<div  class="project_img small_sticker" style="height:220px; width:auto; background-color:#fff; background-image: url({{url('assets/equips/'.$eq->image)}});background-repeat: no-repeat;
        background-position: center center; background-size: cover;">
        <!-- 終了 Complete, '達成':'募集中' 'Status':'Live' -->
    </div>
    <div id="{{$eq->price_main}}" style="cursor: pointer">
        <div style="font-size: 14px; height: 40px"><b>{{$eq->name}}</b></div>
        <div class="pb-2">{{$eq->categoryForeign->name}}</div>
        <div>
            <span class="dims">l: {{$eq->length}}</span>
            <span class="dims">W: {{$eq->width}}</span>
            <span class="dims">H: {{$eq->height}}</span>
        </div>
        <div class="pb-2 text-right" style="font-size: 16px">{{$eq->price_main}}</div>
        <div class="pb-2" style="">Available items: {{$eq->available_stock}}</div>
        <div class="pb-2" style="">
            @if($eq->color_1)
                <span class="colorSpans">{{$eq->color1->name}}</span>
            @endif
            @if($eq->color_2)
                <span class="colorSpans">{{$eq->color2->name}}</span>
            @endif
            @if($eq->color_3)
                <span class="colorSpans">{{$eq->color3->name}}</span>
            @endif
        </div>
    </div>--}}
    <div style="display: none">
        <input type="checkbox" id="{{$eq->id}}" name="{{$eq->id}}" value="{{$eq->id}}">
    </div>
</div>