<?php
// echo '<pre>';
// print_r($equips);
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
    .real_table_td{
        border: 1px solid;
        padding: 4px;
        border-top: 1px solid gray;
        border-bottom: 1px solid gray;
        font-size: 12px
    }
    </style>
    <style type="text/css">
		body {
			font-family: jp_font;
		}

        tr.noBorderfirst td {
            border-bottom: hidden;
        }
        tr.noBorder td {
            border-bottom: hidden;
            border-top: hidden;
        }
        tr.noBorderlast td {
            border-top: hidden;
        }
	</style>
</head>
<body style="padding-top: 40px">
<div>
    <table style="border-collapse: collapse; width: 730px; table-layout:fixed;">
        <tr>
            <td style="border: 1px solid">
                <div style="text-align: right; padding: 5px; font-size: 11px">
                    <div><b>{{$request->company_name}}</b></div>
                    <div><b>{{$request->company_address}}</b></div>
                    <div><b>{{$request->phone_number}}</b></div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid">
                <div style="text-align: center; padding: 5px; font-size: 20px">
                    <div><b>御見積書</b></div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid; font-size: 14px; padding: 0px; padding-left: 5px">
                <table style="width: 100%; border-collapse: collapse">
                    <tr style="padding: 0px">
                        <td style="width: 70%; font-size: 14px; padding: 0px">
                            <div>{{$request->com_name}}</div>
                            <div>{{$request->com_address}}</div>
                            @if($request->com_phone_number != '' && $request->com_phone_number != null)
                                <div>電話: {{$request->com_phone_number}}</div>
                            @endif
                            @if($request->person_incharge != '' && $request->person_incharge != null)
                                <div>{{$request->person_incharge}}様</div>
                            @endif
                            <div style="padding-bottom: 4px">{{$request->email}}</div>
                        </td>
                        <td style="width: 30%; font-size: 12px; border-left: 1px solid; word-wrap:break-word; text-align: center; padding: 0px">
                            <div class="text-center" style="width: 100%">
                                <div class="text-center">番号: {{$request->quatation_number}}</div>
                                <hr style="border: 1px solid #7d7d7d"/>
                                @php
                                    date_default_timezone_set("Asia/Tokyo");
                                    $date = date("Y-m-d");    
                                    $date_timestamp = strtotime($date);
                                    $day = date("d", $date_timestamp);
                                    $month = date("m", $date_timestamp);
                                    $year = date("Y", $date_timestamp);
                                @endphp
                                <div class="text-center">日付: {{$year}}年{{$month}}月{{$day}}日</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
                <?php $table_number = 0; $flag = 0;?>
                <?php $index = 0; $total = 0; $total_with_vat = 0;?>
                @for($i = 0 ; $i < $num_of_total_eqps ; ++$i)
                <tr>
                    <td style="padding: 0px; font-size: 14px">
                    <table style="width: 100%; border-collapse: collapse; font-size: 12px">
                        @if($i == 0)
                            <tr>
                                <!-- <th class="real_table_td"></th>
                                <th class="real_table_td">No</th>
                                <th class="real_table_td">Name</th>
                                <th class="real_table_td">Picture</th>
                                <th class="real_table_td">Size</th>
                                <th class="real_table_td">Color</th>
                                <th class="real_table_td">Quantity</th>
                                <th class="real_table_td">Unit price</th>
                                <th class="real_table_td">Total Price</th> -->
                                <th class="real_table_td">#</th>
                                <th class="real_table_td">品番</th>
                                <th class="real_table_td" style="">品名</th>
                                <th class="real_table_td">写真</th>
                                <th class="real_table_td">仕様</th>
                                <th class="real_table_td">数量</th>
                                <th class="real_table_td">価格</th>
                            </tr>
                        @endif
                        
                        <?php 
                            $total_with_vat += $totol_eqps[$i]->price_main * 1.1; 
                            $total += $totol_eqps[$i]->price_main;
                        ?>
                        <tr>
                            <td width="5%" class="real_table_td">{{$i+1}}</td>
                            <td width="12%" class="real_table_td">{{$totol_eqps[$index]->model_id}}</td>
                            <td width="25%" class="real_table_td" style="word-wrap:break-word">{{$totol_eqps[$i]->name}}</td>
                            <td width="20%" class="real_table_td" style="text-align: center"><img src="{{public_path('assets/equips/'.$totol_eqps[$i]->image)}}" style="height: 70px; width: 70px; onject-fit: cover" /></td>
                            <td width="20%" class="real_table_td" style="font-size: 10px !important">
                                <div>L * H * W</div>
                                <div>{{$totol_eqps[$i]->length}} * {{$totol_eqps[$i]->height}} * {{$totol_eqps[$i]->width}}</div>
                                <div>{{$totol_eqps[$i]->weight}} kg</div>
                            </td>
                            <td width="8%" class="real_table_td" style="text-align: center">{{$count_items[$totol_eqps[$i]->id]}}</td>
                            <td width="10%" class="real_table_td" style="text-align: center">¥{!!floor($totol_eqps[$i]->price_main * (1) * $count_items[$totol_eqps[$i]->id])!!}</td>
                        </tr>
                        
                        @if($i == ($num_of_total_eqps-1))
                            <tr>
                                <td class="real_table_td" colspan="6" style="text-align: right; padding-right: 35px">合計価格</td>
                                <td class="real_table_td" style="text-align: center">¥{!!floor($request->total)!!}</td>
                            </tr>
                            <tr>
                                <td class="real_table_td" colspan="6" style="text-align: right; padding-right: 35px">税込価格</td>
                                <td class="real_table_td" style="text-align: center">¥{!!floor($request->total_vat)!!}</td>
                            </tr>
                            @if($request->discount > 0)
                                <tr>
                                    <td class="real_table_td" colspan="6" style="text-align: right; padding-right: 35px">割引価格</td>
                                    <td class="real_table_td" style="text-align: center">¥{!!floor($request->total_discount)!!}</td>
                                </tr>
                            @endif
                        @endif
                          
                    </table> 
                    </td>
                </tr>
                @endfor

            
        
        <tr><td class="real_table_td">&nbsp;</td></tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">フレームカラー: <span class="ml-5">{{$request->frame_color}}</span></td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">シートカラー: <span class="ml-5">{{$request->seat_color}}</span></td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">備考: <span class="ml-5">{{$request->remarks}}</span></td>
        </tr>
        <tr>
            <td class="real_table_td"><br/></td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">見積有効期限{{" : ".$request->expiration_date}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">納品日{{" : ".$request->delivery_date}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">支払方法{{" : ".$request->payment_method}}</td>
        </tr>
        
        <tr>
            <td class="real_table_td" style="border-top: 1px solid; font-size: 14px">銀行口座情報</td>
        </tr>
        
        <tr>
            <td class="real_table_td" style="font-size: 14px">銀行名{{" : ".$request->bank_name}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">支店名{{" : ".$request->branch_name}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">口座種類{{" : ".$request->account_type}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">口座番号{{" : ".$request->account_number}}</td>
        </tr>
        <tr>
            <td class="real_table_td" style="font-size: 14px">口座名義{{" : ".$request->account_holder}}</td>
        </tr>
        <tr class="noBorderfirst">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    上記金額に国内外の送料、通関料等の配送費は含まれておりません
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    配送費の金額は御見積書に記載をさせて頂きます。金額はご注文の量により変わります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    ※クレーン車、フォークリフト等の機械が必要となる場合は別途料金になります。（弊社で手配も可能です。）
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    また、梱包材の処分はオーナー様にお願いしておりますが配送業者が持ち帰る（有料）ことも可能です。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                組立については、ほとんどのマシンは組立済の状態での納品ですが、マシンの大きさによってコンテナに入らない商品、組立が比較的簡単な商品におきましては、未組み立ての状態で送らせて頂きます。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    基本的にお客様で組み立てて頂きますが、有料の組み立てサービスもご利用いただけます。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                
                    ただし事前の予約が必要になります
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                搬入については、基本的に車上渡しになります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                （ビルなどの場合、ビル前の道路や駐車場）
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                    基本的に20フィートのコンテナ車で送らせて頂きますので、搬入時に十分なスペースがあるかどうか搬入経路などをお客様自信で確認して頂く必要がございます。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                ※重機物になりますので、搬入の際は十分に注意して頂く必要があります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                ●商品保証関係について
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                保証サービスの内容はお見積書に記載致します。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                保証内容は、商品の部品交換のみとなっておりますので、業者に依頼した際に発生した取付け工賃などは、保証対象外となります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                ●単品でのご購入
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                弊社は、基本的に新規店舗のオープン、もしくは、拡大などで、多数のマシンをご購入して頂くオーナー様をメインに考えておりますので、単品での御購入のお客様においては、いろいろご不便をおかけする場合があります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                例えば、単品での御購入は他のお客様の御注文に合わせての発送になりますので、納期の期間が遅れてしまう可能性がございます。理由といたしましては、１個のコンテナを貸切って商品を輸送するのですが、
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                コンテナが２／３以上埋まらないと発送が出来ませんので、他のお客様の御注文とご一緒に発送という形をとっているからです。そのため、他のお客様がいつ頃、注文をして頂くかによって、お客様への納品日が変動してしまう恐れがあります。
            </td>
        </tr>
        <tr class="noBorder">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                また、送料につきましても、単体の商品は割高になります。。
            </td>
        </tr>
        <tr class="noBorderlast">
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                これらの点をご理解いただいたうえでの単品の注文をお考えいただきたいと思います。
            </td>
        </tr>
        
    </table>

</div>
</body>
</html>

