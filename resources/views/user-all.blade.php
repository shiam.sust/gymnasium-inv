@extends('navbar')

@section('custom_css')
    <style>
        .dims{
            font-size: 12px;
            color: gray
        }
        .colorSpans{
            font-size: 14px;
            padding-right: 4px;
            color: gray
        }

        .submit_button{
            border: 1px solid #81a6b7;
            padding: 3px;
            width: 100px;
            border-radius: 4px;
            background-color: white;
            color: gray;
        }

        .cat_back{
            border: 1px solid #3c3c3c;
            background-color: #333333;
            box-shadow: 1px 2px 2px 2px #3c3c3c;
            padding-left: 10px;
        }

        .edit-butt{
            border: 1px solid;
            border-radius: 4px
        }

        .buttons{
            /* border: 1px solid #7f9098; */
            border-radius: 4px;
            background-color: white;
            width: 190px;
            padding: 5px;
            box-shadow: 1px 1px 1px 1px #7f9098;
        }

        .btn-size{
            width: 80px;
            background-color: #FFFFFF;
            color: #7f9098;
        }
    </style>
@stop



@section('content')
   <div class="col-md-12">
        <form class="col-md-12 pl-0 pt-2" id="add_user_form" action="{{route('post-new-user')}}" method="post">
            {{ csrf_field() }}
            <div class="col-md-12 row mb-3">
                <div class="col-md-2">会員名</div>
                <div class="col-md-10"><input id="name" name="name" style="width: 100%; text-align: left"/>
                    <div id="name_error" style="color: red; display: none"><span>please enter your name</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3">
                <div class="col-md-2">メールアドレス</div>
                <div class="col-md-10">
                    <input type="email" id="email" name="email" style="width: 100%; text-align: left"/>
                    <div id="email_error" style="color: red; display: none"><span>please enter your email</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3">
                <div class="col-md-2">パスワード</div>
                <div class="col-md-10">
                    <input type="password" id="password" name="password" style="width: 100%; text-align: left"/>
                    <div id="password_error" style="color: red; display: none"><span>please enter your password</span></div>
                </div>
            </div>
            <div class="row col-md-12">
                <div class="col-md-2">会員権</div>
                <div class="col-md-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="user_type" id="exampleRadios1" value="1">
                        <label class="form-check-label" for="exampleRadios1">
                            管理者
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="user_type" id="exampleRadios2" value="2">
                        <label class="form-check-label" for="exampleRadios2">
                            会員
                        </label>
                    </div>
                    <div id="user_type_error" style="color: red; display: none"><span>please select user type</span></div>
                </div>
                
            </div>
        </form>
        <div class="col-md-12 text-center mb-5">
            <button id="post_user" class="btn mt-5 buttons btn-size" style="width: 120px">会員追加</button>
        </div>
        <div class="col-md-12 text-center mt-5">
            <h4>会員一覧</h4>
        </div>
        <div style="overflow-x:auto;">
            <table class="table table-dark" style="min-width: 1000px">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">会員名</th>
                    <th scope="col">メールアドレス</th>
                    <th scope="col">パスワード</th>
                    <th scope="col">会員権</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $k => $data)
                        <tr>
                            <th scope="row">{{ $k+1 }}</th>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->password_raw}}</td>
                            <td>
                                @if($data->type == 1)
                                    管理者
                                @elseif($data->type == 2)
                                    会員
                                @endif
                            </td>
                            <td>
                                @if($data->id != 6)
                                    <a href="{{route('delete-user', ['id' => $data->id])}}" onclick="return confirm('会員追加します。よろしいですか？')">
                                        <img src="{{Request::root()}}/assets/img/delete.png" title="instagram" style="height: 15px; width: 15px"/>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
   </div>
@stop

@section('custom_js')
    
<script>
    $(document).ready(function() {
        $("#post_user").click(function(){
            var flag = 0;
            
            if($('#name').val() == '' || $('#name').val() == null){
                flag = 1;
                $('#name_error').show();
            }

            if($('#email').val() == '' || $('#email').val() == null){
                flag = 1;
                $('#email_error').show();
            }

            if($('#password').val() == '' || $('#password').val() == null){
                flag = 1;
                $('#password_error').show();
            }

            var hh = $("input[name='user_type']:checked").val();
            console.log(hh);

            if(hh == undefined){
                flag = 1;
                $('#user_type_error').show();
            }

            if(flag == 0){
                $("#add_user_form").submit();
            }
        }); 

        
       
    });
    
</script>
@stop