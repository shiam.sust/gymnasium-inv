@extends('navbar')

@section('custom_css')
    <style>
        .dims{
            font-size: 12px;
            color: gray
        }
        .colorSpans{
            font-size: 14px;
            padding-right: 4px;
            color: gray
        }

        .table-border{
            border: 1px solid gray;
            padding: 10px;
            text-align: center;
        }

        .series_filter{
            width: 100%;
            border: 1px solid gray;
            border-radius: 4px;
            height: 25px;
        }
    </style>
@stop


@section('content')
    <div>
        <div class="col-md-12 alternates" style="">
            <div class="col-md-12 col-sm-12">
                <div class="" style="">
                    <table style="width: 100%; ">
                        <tr>
                            <td style="width: 100%; padding: 0px">
                                <div class="col-md-12 p-0 row">
                                    <div class="col-md-10 p-0" style="text-align: left; font-size: 20px; padding-bottom: 10px"><b>{{$title}}</b></div>
                                    <div class="col-md-2 p-0 pb-2">
                                        <form action="{{route('view-products')}}">
                                            <div>
                                                <select class="series_filter" id="series" name="series" onchange="this.form.submit()">
                                                    <option value="">シリーズ選択</option>
                                                    @foreach($series as $sr)
                                                        <option value="{{$sr->id}}" <?php if($selected_series == $sr->id){echo ' selected="selected"';} ?>>{{$sr->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>    
        <div class="col-md-12 alternates" style="">
            <div class="col-md-12 col-sm-12" style="overflow-x:auto;overflow-y:hidden" >
                <table class="table-dark" style="width: 100%; border: 1px solid;min-width:1100px">
                    <tbody>
                        <tr>
                            <th class="table-border" style="width: 8%">品番</th>
                            <th class="table-border" style="min-width:150px">品名</th>
                            <th class="table-border">写真</th>
                            <th class="table-border">仕様</th>
                            <th class="table-border">原価</th>
                            <th class="table-border">円換算</th>
                            <th class="table-border">卸価格</th>
                            <th class="table-border">希望小売価格</th>
                            <th class="table-border" style="width: 30%; min-width:170px">仕様</th>
                        </tr>
                        @foreach($products as $pro)
                            <tr>
                                <td class="table-border">
                                    <div>{{$pro->model_id}}</div><br/>
                                    <div>
                                        <form action="{{route('edit-equipment', ['id' => $pro->id])}}">
                                            <button style="border: 1px solid white; border-radius: 4px; background-color: white; width: 60px">編集</button>
                                        </form>
                                    </div>
                                </td>
                                <td class="table-border" style="width:120px">{{$pro->name}}</td>
                                <td class="table-border"><img src="{{Request::root()}}/assets/equips/{{$pro->image}}" style="height: 70px; width: 70px; onject-fit: cover" /></td>
                                <td class="table-border" style="text-align: left; width: 140px">
                                    <div>
                                        <div>寸法（mm）:</div>
                                        <div>(L){{$pro->length}}*(W){{$pro->width}}*(H){{$pro->height}}</div>
                                        <div>N.W.:{{$pro->weight}}kgs</div>
                                    </div>
                                </td>
                                <td class="table-border">${{$pro->buying_cost_dollar}}</td>
                                <td class="table-border">¥{{$pro->buying_cost_yen}}</td>
                                <td class="table-border">¥{{$pro->wholesale_price}}</td>
                                <td class="table-border">¥{{$pro->price_main}}</td>
                                <td class="table-border">{{$pro->comment}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('custom_js')

@stop