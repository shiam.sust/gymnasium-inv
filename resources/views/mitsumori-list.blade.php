@extends('navbar')

@section('custom_css')
    <style>

    </style>
@stop



@section('content')
    <div class="col-md-12 row pt-5">
        <div class="col-md-12 p-2" style="font-size: 16px; overflow-x:auto;">
            <table class="table-dark" style="width: 100%; border: 1px solid white; min-width:1100px">
                <tr>
                    <th style="border: 1px solid white; text-align: center">作成日</th>
                    @if($show == 1)
                        <th style="border: 1px solid white; text-align: center">会員名</th>
                        <th style="border: 1px solid white; text-align: center">メール</th>
                    @endif
                    <th style="border: 1px solid white; text-align: center">見積書</th>
                </tr>
                @foreach($mitsumori as $sr)
                <tr onclick="window.open('assets/mitsumori/{{$sr->invoice_name}}.pdf', '_blank')" style="cursor: pointer" target="_blank">
                    <td style="width: 30%; border-bottom: none; border: 1px solid white">{{$sr->created_at}}</td>
                    @if($show == 1)
                        <td style="width: 35%; border-bottom: none; border: 1px solid white">{{$sr->creator->name}}</td>
                        <td style="width: 35%; border-bottom: none; border: 1px solid white">{{$sr->creator->email}}</td>
                    @endif
                    <td style="width: 35%; border-bottom: none; border: 1px solid white">{{$sr->invoice_name}}.pdf</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop

@section('custom_js')
    <script>
    
    </script>
@stop