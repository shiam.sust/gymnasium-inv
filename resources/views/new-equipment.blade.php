@extends('navbar')

@section('custom_css')
    <style>
        .labels{
            font-size: 14px;
            text-align: left;
            color: white
        }

        /* @media only screen and (max-width: 600px) { */
            .input-fields {
                width: 100%;
            }
        /* } */
    </style>
@stop

@section('content')
    <div class="col-md-12 alternates p-0" style="width: 100%; max-width: 600px !important">
        <div class="col-md-12 col-sm-12 p-0">
        <form id="equip-form" action="{{route('upload-form')}}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
            <!-- category -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">カテゴリ</div>
                <div class="col-md-10 text-left" onchange="hide('category','category_error')">
                    <select class="input-fields" name="category" id="category">
                        <option value="">カテゴリ選択</option>
                        @foreach($cats as $ct)
                            <option value="{{$ct->id}}" {{ (isset($equip) && $equip->category == $ct->id) ? 'selected' : '' }}>{{$ct->name}}</option>
                        @endforeach
                    </select>
                    <div id="category_error" style="color: red; display: none"><span><b>カテゴリを選択して下さい！</b></span></div>
                </div>
            </div>
            <br/>
            <!-- series -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">シリーズ</div>
                <div class="col-md-10 text-left" onchange="hide('series','series_error')">
                    <select class="input-fields" name="series" id="series" >
                        <option value="">シリーズ選択</option>
                        @foreach($series as $s)
                            <option value="{{$s->id}}" {{ (isset($equip) && $equip->series == $s->id) ? 'selected' : '' }}>{{$s->name}}</option>
                        @endforeach
                    </select>
                    <div id="series_error" style="color: red; display: none"><span><b>シリーズを選択して下さい！</b> </span></div>
                </div>
            </div>
            <br/>
            <!-- title -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">品名</div>
                <div class="col-md-10 text-left " onchange="hide('title','title_error')">
                    <input type="text" class="input-fields" name="title" id="title" value="{{ isset($equip) ? $equip->name : '' }}"/>
                    <div id="title_error" style="color: red; display: none"><span><b>品名を入力して下さい！</b></span></div>
                </div>
            </div>
            <br/>
            <!-- title -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">品番</div>
                <div class="col-md-10 text-left" onchange="hide('model_id','model_error')">
                    <input type="text" class="input-fields" name="model_id" id="model_id" value="{{ isset($equip) ? $equip->model_id : '' }}"/>
                    <div id="model_error" style="color: red; display: none"><span><b>品番を入力して下さい！</b></span></div>
                </div>
            </div>
            <br/>

            <!-- title -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">写真</div>
                <div class="col-md-10 text-left">
                    <input type="file" name="image" id="image" />
                    <div id="image_error" style="color: red; display: none"><span>画像を入力してください！</span></div>
                    <div id="image_size_error" style="color: red; display: none"><span>画像サイズを軽くして下さい！</span></div>
                </div>
            </div>
            <br/>

            @if(isset($equip))
                <div class="col-md-12 row">
                    <input type="hidden" id="prev_id" name="prev_id" value="{{$equip->id}}" />
                    <div class="col-md-2 labels"></div>
                    <div class="col-md-10 labels">
                        <img src="{{url('/assets/equips/'.$equip->image)}}" style="width: 150px; height: auto"/>
                    </div>
                </div>
                <br/>
            @endif
            
            <!-- dimentions -->
            

            <div class="col-md-12 row">
                <div class="col-md-2 labels">寸法 L</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="length" name="length" value="{{ isset($equip) ? $equip->length : '' }}"/>
                </div>
            </div>
            <br/>
            <div class="col-md-12 row">
                <div class="col-md-2 labels">寸法 W</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="width" name="width" value="{{ isset($equip) ? $equip->width : '' }}"/>
                </div>
            </div>
            <br/>
            <div class="col-md-12 row">
                <div class="col-md-2 labels">寸法 H</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="height" name="height" value="{{ isset($equip) ? $equip->height : '' }}"/>
                </div>
            </div>
            <br/>

            <!-- weight -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">重量</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="weight" name="weight" value="{{ isset($equip) ? $equip->weight : '' }}"/>
                </div>
            </div>
            <br/>

            <!-- price -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">原価</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="price_usd" name="price_usd" value="{{ isset($equip) ? $equip->buying_cost_dollar : '' }}"/>
                </div>
            </div>
            <br/>
            <!-- price -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">円換算</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="price_yen" name="price_yen" value="{{ isset($equip) ? $equip->buying_cost_yen : '' }}"/>
                </div>
            </div>
            <br/>
            <!-- price -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">卸価格</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="price_wholesale" name="price_wholesale" value="{{ isset($equip) ? $equip->wholesale_price : '' }}"/>
                </div>
            </div>
            <br/>
            <!-- price -->
            <div class="col-md-12 row">
                <div class="col-md-2 labels">小売価格</div>
                <div class="col-md-10 text-left">
                    <input class="input-fields" id="price" name="price" value="{{ isset($equip) ? $equip->price_main : '' }}"/>
                </div>
            </div>
            <br/>

            
            <div class="col-md-12 row">
                <div class="col-md-2 labels">考察</div>
                <div class="col-md-10 text-left">
                    <textarea rows="6" type="text" class="input-fields" name="comment" id="comment">{{ isset($equip) ? $equip->comment : '' }}</textarea>
                </div>
            </div>
            <br/>
            <br/>        

            
            </form>
            <div class="col-md-12 text-center">
                <button id="send_eqp" class="buttons">アップロード</button>
            </div>
        </div>
    </div>
    
    
@stop

@section('custom_js')

<script>

    function hide(id,error){
        if($('#'+id).val() == '' || $('#'+id).val() == null){
            $('#'+error).show();
        }else{
            $('#'+error).hide();
        }
    }

    $(document).ready(function() {
        var flag = 0;

        // $('#image').bind('change', function() {

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].size);
            // console.log(this.files[0]);
            // var img_size = this.files[0].size;
            // if(img_size > 2000000){
            //     flag = 1;
            //     $('#image_size_error').show();
            // }else{
            //     flag = 0;
            //     $('#image_size_error').hide();
            // }

        // });
        var _URL = window.URL || window.webkitURL;
        $("#image").change(function (e) {
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (height > 2000 || width > 2720) {
                        $('#image').val('');
                        $('#image_size_error').show();
                        return false;
                    }else{
                        $('#image_size_error').hide();
                        return true;
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });                                 

        $("#send_eqp").click(function(){
            var flag = 0;
            if($('#category').val() == '' || $('#category').val() == null){
                flag = 1;
                $('#category_error').show();
            }
            if($('#series').val() == '' || $('#series').val() == null){
                flag = 1;
                $('#series_error').show();
            }
            if($('#title').val() == '' || $('#title').val() == null){
                flag = 1;
                $('#title_error').show();
            }
            if($('#model_id').val() == '' || $('#model_id').val() == null){
                flag = 1;
                $('#model_error').show();
            }
            if(($('#image').val() == '' || $('#image').val() == null) && ($('#prev_id').val() == '' || $('#prev_id').val() == null)){
                flag = 1;
                $('#image_error').show();
            }
            
            if(flag == 0){
                if(confirm("こちらの内容でよろしいですか！")){
                    $("#equip-form").submit();
                }
            }
        }); 

       
    });
</script>
@stop