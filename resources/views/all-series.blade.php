@extends('navbar')

@section('custom_css')
    <style>
        .dims{
            font-size: 12px;
            color: gray
        }
        .colorSpans{
            font-size: 14px;
            padding-right: 4px;
            color: gray
        }

        .submit_button{
            border: 1px solid #81a6b7;
            padding: 3px;
            width: 100px;
            border-radius: 4px;
            background-color: white;
            color: gray;
        }

        .cat_back{
            border: 1px solid #3c3c3c;
            background-color: #333333;
            box-shadow: 1px 2px 2px 2px #3c3c3c;
            padding-left: 10px;
        }

        .edit-butt{
            border: 1px solid;
            border-radius: 4px
        }
    </style>
@stop



@section('content')
    <form id="series_form" action="{{route('add-series')}}" method="POST">
        <div class="col-md-12 row ">
            {{ csrf_field() }}
            <input type="hidden" id="edit_id" name="edit_id" />
            <div class="col-md-2 pb-2">シリーズ名入力</div>
            <div class="col-md-7 pb-2"><input id="series" name="series" style="width: 70%; border: 1px solid; border-radius: 4px; height: 27px; padding: 6px"/></div>
            <div class="col-md-3 text-left"></div>
        </div>
        <div class="col-md-12 row">
            <div class="col-md-2 pb-2"></div>
            <div class="col-md-7 pb-2"><div id="series_error" style="color: red;display:none"><span>シリーズ名を入れて下さい！</span></div></div>
            <div class="col-md-3 text-left"></div>
        </div>
    </form>
    <div class="col-md-12 row">
        {{-- <div class="col-md-2 pb-2"></div> --}}
        <div class="col-md-9 col-sm-8 col-8 pb-2 text-center"><button id="series_submit" class="submit_button">追加</button></div>
        {{-- <div class="col-md-3 text-left"></div> --}}
    </div>
    
    <div class="col-md-12 row pt-5">
        @foreach($all_series as $sr)
            <div class="col-md-6 p-2" style="font-size: 16px;">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%; border-bottom: none;">{{$sr->name}}</td>
                        <td style="width: 50%; border: none; padding-left: 30px"><button class="edit-butt" onclick="load({{$sr->id}}, '{{$sr->name}}')">編集 </button></td>
                    </tr>
                </table>
            </div>
        @endforeach
    </div>
@stop

@section('custom_js')
    <script>
        function load(id, name)
        {
            $('#edit_id').val(id);
            $('#series').val(name);
        }

        $("#series_submit").click(function(){

            var flag = 0;
            
            if($('#series').val() == '' || $('#series').val() == null){
                flag = 1;
                $('#series_error').show();
            }
            
            if(flag == 0){
                if(confirm("こちらの内容でよろしいですか！")){
                    $("#series_form").submit();
                }
            }
        }); 
    </script>
@stop