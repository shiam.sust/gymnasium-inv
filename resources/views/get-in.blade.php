@extends('navbar')

@section('custom_css')
<style>
   .input_box{
        border: 1px solid #b5b5b5;
        border-radius: 3px;
        width: 70%;
        height: 35px;
   }

   .submit_button{
        border: 1px solid #81a6b7;
        padding: 5px;
        width: 75px;
        border-radius: 4px;
        background-color: #81a6b7;
        color: white;
   }
</style>
@stop


@section('content')
    <div class="col-md-12 alternates p-0" style="max-width: 500px !important">
        <div class="col-md-12 col-sm-12 text-center mt-5 p-0">
            <form action="{{route('loggin')}}" method="post">
                {{ csrf_field() }}
                <div style="width: 100%; border: 1px solid #dadada; min-height: 300px; text-center; padding-top: 10%">
                    <div class="m-3 text-left" style="padding-left: 14%; color: white; margin-bottom: 4px">ユーザ名</div>
                    <div class="m-3"><input class="input_box pl-2" id="name" name="name"/></div>
                    <div class="m-3 text-left" style="padding-left: 14%; color: white">パスワード</div>
                    <div class="m-3"><input type="password" class="input_box pl-2" id="password" name="password"/></div>
                    <div class="text-center mt-5 mb-5">
                        <button class="submit_button" style="width: 100px; background-color: white; color: gray">ログイン</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('custom_js')

@stop