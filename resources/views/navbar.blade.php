<!DOCTYPE html>
<html lang="en" style="">
	<head>
		<title>Invoice</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="{{Request::root()}}/assets/style.css">
        <link rel="stylesheet" type="text/css" href="{{Request::root()}}/assets/bootstrap-datepicker.css">
        
        {{-- <link href="{{Request::root()}}/assets/admin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" /> --}}
        {{-- <link href="{{Request::root()}}/admin/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" />
        <link href="{{Request::root()}}/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="{{Request::root()}}/admin/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <link href="{{Request::root()}}/css/bootstrap-datetimepicker.min.css" id="theme" rel="stylesheet"> --}}

    


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
      
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
        {{-- <script src="{{Request::root()}}/assets/admin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> --}}
        {{-- <script src="{{Request::root()}}/assets/admin/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="{{Request::root()}}/assets/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="{{Request::root()}}/assets/js/bootstrap-datetimepicker.min.js"></script> --}}

        <script src="{{Request::root()}}/assets/bootstrap-datepicker.js"></script>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js" integrity="sha256-2JRzNxMJiS0aHOJjG+liqsEOuBb6++9cY4dSOyiijX4=" crossorigin="anonymous"></script>
		
        <style type="text/css">
            body{
                font-size: 12px !important;
                background-color: #f9f9f9;
            }
            .btn-no-border-radius{
                border-radius: 0px !important;
            }
            .border-right{
                border-right: 1px solid #ced4da;
            }
            .front_header{
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .margin-top{
                margin-top: 15px;
            }
            .bg-white{
                background-color: #FFF;
            }
            .panel-auth{
                padding: 10px;
            }
            .facebook{
                background-color: #3B5997 !important;
                border-radius: 0px !important;
            }
            .google{
                background-color: #D34735 !important;
                border-radius: 0px !important;
            }
            .twitter{
                background-color: #3884B4 !important;
                border-radius: 0px !important;
            }
            .auth_area{
                background-color: #F1F1F1;
            }
            .front_header{
                background-color: #ffffff;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .auth_page_title{
                padding-top: 15px;
                padding-bottom: 15px;
            }
            .auth_page_title h1{
                font-size: 20px;
            }
            .auth_area{
                padding-bottom: 100px;
            }
            .auth_form_area .area_auth{
                padding-top: 50px;
            }
            .auth_form_area .area_auth .form-group{
                margin-top: 20px;
            }
            .auth_form_area .part_1 h2, .part_2 h2{
                font-size: 16px;
            }
            .part_2 h2{
                margin-bottom: 30px;
            }
            .part_1, .part_2{
                padding: 40px;
                padding-top: 0px;
            }
            .btn-primary{
                background-color: #618ca9;
                border-color: #618ca9;
            }
            .auth_form_area .part_1{
                position: relative;
            }
            .auth_form_area .part_1:after{
                /* content: "";
                display: block;
                width: 1px;
                height: 100%;
                position: absolute;
                right: 0px;
                top:0px;
                background-color: #C6C6C6; */
            }
            .area_auth{
                padding-bottom: 50px !important;
            }
            .btn{
                cursor: pointer;
            }
            .custom_pad{
                line-height: 20px !important;
                font-size: 13px !important;
                font-weight: 800 !important;
            }
            .badge-noti{
                float: right !important;
                margin-bottom: -6px !important;
                margin-top: 12px !important;
                margin-left: -10px !important;
            }

            .badge_noti_mobile{
                margin-left: 9px !important;
                float: left !important;
                margin-top: 0px !important;
                border-radius: 50% !important
            }
            
            .dropdown-menu {
                width: 170px !important;
                
            }

            .buttons{
                border: 1px solid gray;
                border-radius: 4px;
                background-color: white;
                width: 190px;
                padding: 5px;
                box-shadow: 1px 1px #7f9098;
                color: gray;
                outline: none !important;
                border: none !important;
            }
            .mytabs{
                /* width: 33%; */
                text-align: center;
                /* border: 1px solid gray; */
                padding: 20px;
            }
            .table_cells{
                width: 23%;
                text-align: center;
                border: 1px solid;
                padding: 9px;
            }
            .index_cells{
                width: 8%; 
                border: 1px solid; 
                text-align: center;
            }
            td {
                vertical-align: top;
                border-bottom: 1px solid #7d7d7d;
                padding: 10px
            }
            .btn-size{
                width: 80px;
                background-color: #FFFFFF;
                color: #7f9098;
            }

            button{
                color: black !important
            }

            @media only screen and (max-width: 600px) {
                .small_nav {
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                }
            }

            .condigo_fondos{
                background:
                    radial-gradient(black 15%, transparent 16%) 0 0,
                    radial-gradient(black 15%, transparent 16%) 8px 8px,
                    radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,
                    radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;
                background-color:#282828;
                background-size:16px 16px;
                color: white
            }

            .alternates{
                margin: 0 auto !important; 
                max-width: 1150px !important;
            }

            .text-16{
                font-size: 16px;
            }
            .text-25{
                font-size: 25px;
            }
            .text-14{
                font-size: 14px;
            }
            .text-12{
                font-size: 12px;
            }
            .text-10{
                font-size: 10px;
            }
            /* for dropdown */
            .dropdown-menu{
                top: -20px !important
            }
            a:focus {
                background-color: white !important;
            }
            
            

        </style>
        <style type="text/css">
            body {
                font-family: jp_font;
            }
        </style>
        @yield('custom_css')
	</head>
	<body>
        <div id="navbar" class="navigation_bar col-md-12 row condigo_fondos" style="padding: 0px; border-bottom: 2px solid white">
            <div class="col-md-12 alternates small_nav" >
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 50%; text-align: left; padding: 0px; font-size: 22px; border-bottom: 0px !important; color: white; "><a href="{{route('front-home')}}" style="color: white !important"><img src="{{url('/assets/img/logo.jpg')}}" style="width: 150px; height: 45px"/></a></td>
                            <td style="width: 50%; text-align: right; padding: 0px; border-bottom: 0px !important; color: white !important; ">
                                <div id="profile-menu" class="btn-group">
                                    @if(isset(Auth::user()->id))
                                        @if(Auth::user()->type == 1)
                                            <a class="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer; color: white !important">
                                                管理者
                                            </a>
                                        @else
                                            <a class="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor: pointer; color: white; !important">
                                                会員
                                            </a>
                                        @endif
                                        <div class="dropdown-menu dropdown-menu-right custom_pad" style="color: black !important">
                                            @if(Auth::user()->type == 1)<a class="dropdown-item" type="" href="{{route('upload-eqip')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">商品アップ</a>@endif
                                            @if(Auth::user()->type == 1)<a class="dropdown-item" type="" href="{{route('view-products')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">商品一覧</a>@endif
                                            @if(Auth::user()->type == 1)<a class="dropdown-item" type="" href="{{route('series-management')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">シリーズ追加</a>@endif
                                            @if(Auth::user()->type == 1)<a class="dropdown-item" type="" href="{{route('user-management')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">アカウント追加</a>@endif
                                            @if(Auth::user()->type == 1)
                                                <a class="dropdown-item" type="" href="{{route('mitsumori-list')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">見積書一覧</a>
                                            @else(Auth::user()->type == 1)
                                                <a class="dropdown-item" type="" href="{{route('mitsumori-list-ind')}}" style="margin-bottom: 9px; margin-right: 9px; width: 140px; color: black !important">見積書一覧</a>
                                            @endif
                                            <a class="dropdown-item" type="" href="{{route('logout')}}" style="margin-right: 9px; width: 140px; color: black !important">ログアウト</a>
                                        </div>
                                    @endif

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    
        <div id="body" class="col-md-12 row pt-5 condigo_fondos" style="min-height:745px; padding-bottom: 20px">
            <div class="col-md-12 alternates p-0">
                <div class="col-md-12 col-sm-12 p-0">
                    @yield('content')
                </div>
            </div>
        </div>

        <!-- Footer -->
       
        
        <footer class="flex_cont condigo_fondos pt-5" style="border-top: none; min-height: 111px">
            <div class="col-md-12 row">
                <div class="col-md-2"></div>
                <div class="col-md-8 row" style="color: white">
                </div>
                <div class="col-md-2"></div>
            </div>
        </footer>


       

		@yield('custom_js')

	</body>
</html>

