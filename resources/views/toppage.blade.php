@extends('navbar')

@section('custom_css')
    <style>
        .dims{
            font-size: 12px;
            color: gray
        }
        .colorSpans{
            font-size: 14px;
            padding-right: 4px;
            color: gray
        }

        .submit_button{
            border: 1px solid #ffffff;;
            padding: 5px;
            width: 160px;
            border-radius: 4px;
            background-color: #ffffff;;
            color: #607D8B;
            box-shadow: 2px 2px gray;
        }

        .cat_back{
            border: 1px solid #3c3c3c;
            background-color: #333333;
            box-shadow: 1px 2px 2px 2px #3c3c3c;
            padding-left: 10px;
        }

        .color-divs{
            height: 150px;
            border: 1px solid;
        }

        .color-div-labels{
            text-align: center;
            padding-top: 7px;
        }

        .dropdown-menu {
            width: 50px !important;
        }

        .color-divs{
            border: 1px solid; 
            /* background-color: white */
        }


        .horizontal-scrollable > .row { 
            overflow-x: auto; 
            white-space: nowrap; 
        } 
          
        .horizontal-scrollable > .row > .col-xs-4 { 
            display: inline-block; 
            float: none; 
        } 
        /* Decorations */ 
          
        .col-xs-4 { 
            color: white; 
            font-size: 24px; 
            padding-bottom: 20px; 
            padding-top: 18px; 
        } 
          
        .col-xs-4:nth-child(2n+1) { 
            background: green; 
        } 
          
        .col-xs-4:nth-child(2n+2) { 
            background: black; 
        } 

        #myBtn {
            display: none;
            position: fixed;
            bottom: 25px;
            right: 25px;
            z-index: 1099;
            font-size: 13px;
            border: none;
            outline: none;
            background-color: #9e9e9e;
            color: white !important;
            cursor: pointer;
            padding: 12px;
            border-radius: 50%;
            }

            #myBtn:hover {
            background-color: #555;
        }
    </style>
@stop



@section('content')
<div id="third_part_top pb-3" class="col-md-12 text-center" style="display: none"><h6>入力情報を確認</h6></div>
<div id="third_page_partone" class="col-md-12  text-center horizontal-scrollable" style="border: 1px solid gray; font-size:14px; display:none; padding-left: 0px; padding-right: 0px; overflow-x:auto; border-bottom: none">
    <table style="width: 100%; border-collapse: collapse; min-width: 600px;">
        <tr>
            <td style="">
                <div style="text-align: right; font-size: 11px">
                    <div id="two"><b></b></div>
                    <div id="three"><b></b></div>
                    <div id="four"><b></b></div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="">
                <div style="text-align: center; font-size: 20px">
                    <div><b>見積書</b></div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="border: none; padding: 0px">
                <table style="width: 100%; border-collapse: collapse">
                    <tr style="padding: 0px !important">
                        <td style="width: 70%; font-size: 14px; padding: 0px; padding-left: 5px; border-bottom: none" class="text-left">
                            <div id="five"></div>
                            <div id="six"></div>
                            <div id="seven"></div>
                            <div id="eight"></div>
                            <div id="nine"></div>
                        </td>
                        <td style="width: 30%; font-size: 14px; border-left: 1px solid; padding-left: 0px; padding-right: 0px; border-bottom: none; vertical-align: middle">
                            <div class="text-center" style="width: 100%">
                                <div>番号: <span id="one"></span></div>
                                <hr/>
                                @php
                                    date_default_timezone_set("Asia/Tokyo");
                                    $date = date("Y-m-d");    
                                    $date_timestamp = strtotime($date);
                                    $day = date("d", $date_timestamp);
                                    $month = date("m", $date_timestamp);
                                    $year = date("Y", $date_timestamp);
                                    $final = date("Y-m-d", strtotime("+1 month", $date_timestamp));
                                @endphp
                                <div>日付: {{$year}}年{{$month}}月{{$day}}日</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</div>




<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
<form id="invoice_form" action="{{route('generate-invoice')}}" method="post" style="width: 100%">
    {{ csrf_field() }}

    <div id="table_second" style="display: none;overflow-x:auto">

    </div>

    <div id="first_page">
        @foreach($categories as $cat)
            <div id="{{'catbar'.$cat->id}}" class="col-md-12 row cat_back" onclick="expand({{$cat->id}})">
                <div class="col-md-5" style="font-size: 25px">{{$cat->English_name}}</div>
                <div class="col-md-5 pt-2"><span class="text-16">{{$cat->name}}</span></div>
                <div class="col-md-2"></div>
            </div>
            <div id="{{'show'.$cat->id}}" class="row m-0 other" style="display:none" >
                @foreach($equips as $eq)
                    @if($eq->category == $cat->id)
                        <div class="col-md-6">
                            @include('top-equip')
                        </div>
                    @endif
                @endforeach
            </div>
            <br/>
            <br/>
            <br/>
        @endforeach
            <div class="col-md-12 row cat_back" onclick="expand('frame')">
                <div class="col-md-5" style="font-size: 25px">FRAME COLOR</div>
                <div class="col-md-5 pt-2"><span class="text-16">フレームカラー</span></div>
                <div class="col-md-2"></div>
                {{-- <span  class="text-25 pr-5">ストレングスマシン</span><span class="text-16;">フレームカラー</span> --}}
            </div>
            <div id="{{'show'.'frame'}}" class="row m-0 other mt-4" style="display:none" >
                @foreach($framecolors as $cols)
                    <div class="col-md-2">
                        <div class="color-divs" style="background-image: url({{$cols->image_name}})"></div>
                        <div class="color-div-labels pb-2"><input type="radio" id="{{$cols->name}}" name="frame_color" value="{{$cols->name}}">&nbsp;&nbsp;{{$cols->name}}</div>
                    </div>
                @endforeach
            </div>
            <br/>
            <br/>
            <br/>
            <div class="col-md-12 row cat_back" onclick="expand('seat')">
                <div class="col-md-5" style="font-size: 25px">SHEET COLOR</div>
                <div class="col-md-5 pt-2"><span class="text-16">シートカラー</span></div>
                <div class="col-md-2"></div>
                {{-- <span  class="text-25 pr-5">ストレングスマシン</span><span class="text-16;">シートカラー</span> --}}
            </div>
            <div id="{{'show'.'seat'}}" class="row m-0 other mt-4" style="display:none" >
                @foreach($sheetcolors as $cols)
                    <div class="col-md-2">
                        <div class="color-divs" style="background-image: url({{$cols->image_name}})"></div>
                        <div class="color-div-labels pb-2"><input type="radio" id="{{$cols->name}}" name="seat_color" value="{{$cols->name}}">&nbsp;&nbsp;{{$cols->name}}</div>
                    </div>
                @endforeach
            </div>
    </div>
    

    <div id="second_page" class="col-md-12 p-0" style="display: none; border-top: 0px">
        
        
        <div class="col-md-12 row p-2" style="border: 1px solid; border-top: 0px; background-color: white">
            <div class="col-md-12 pl-0">フレームカラー : <span id="frameclr"></span></div>
        </div>
        <div class="col-md-12 row p-2" style="border: 1px solid; border-top: 0px; background-color: white">
            <div class="col-md-12 pl-0">シートカラー : <span id="seatclr"></span></div>
        </div>
        <div class="col-md-12 row p-2" style="border: 1px solid; border-top: 0px; background-color: white">
            <div class="col-md-2 pl-0">備考</div>
                <div class="col-md-10 pl-0"><input id="remarks" name="remarks" style="width: 97%; text-align: left"/>
            </div>
        </div>
        <div><br/></div>
        <div class="col-md-12" style="border: 1px solid gray; background-color: white">
            <div class="col-md-12 row p-2" style="height: 10px"></div>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">見積番号</div>
                <div class="col-md-10 pl-0"><input id="quatation_number" name="quatation_number" style="width: 100%; text-align: left"/>
                    <div id="quatation_number_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div><span>会社情報入力</span></div><br/>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">会社名</div>
                <div class="col-md-10 pl-0"><input id="company_name" name="company_name" style="width: 100%; text-align: left"/>
                    <div id="company_name_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">会社住所</div>
                <div class="col-md-10 pl-0"><input id="company_address" name="company_address" style="width: 100%; text-align: left"/>
                    <div id="company_address_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">電話番号</div>
                <div class="col-md-10 pl-0"><input id="phone_number" name="phone_number" style="width: 100%; text-align: left"/>
                    <div id="phone_number_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <br/>
            <div><span>クライアント情報入力</span></div><br/>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">会社名</div>
                <div class="col-md-10 pl-0"><input id="com_name" name="com_name" style="width: 100%; text-align: left"/>
                    <div id="company_name_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">会社住所</div>
                <div class="col-md-10 pl-0"><input id="com_address" name="com_address" style="width: 100%; text-align: left"/>
                    <div id="company_address_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">電話番号</div>
                <div class="col-md-10 pl-0"><input id="com_phone_number" name="com_phone_number" style="width: 100%; text-align: left"/>
                    <div id="company_phone_number_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">担当者</div>
                <div class="col-md-10 pl-0"><input id="person_incharge" name="person_incharge" style="width: 100%; text-align: left"/>
                    <div id="person_incharge_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-4 pl-0">
                <div class="col-md-2 pl-0">メール</div>
                <div class="col-md-10 pl-0"><input id="email" name="email" style="width: 100%; text-align: left"/>
                    <div id="email_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            @php
                $y = date('Y', strtotime($final));
                $m = date('m', strtotime($final));
                $d = date('d', strtotime($final));
                $formated_str = $y.'年'.$m.'月'.$d.'日';
            @endphp
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">見積有効期限</div>
                <div class="col-md-10 pl-0">
                    <input id="expiration_date" name="expiration_date" style="width: 100%; text-align: left" value="{{$formated_str}}" readonly/>
                    <div id="expiration_date_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">納品日</div>
                <div class="col-md-10 pl-0"><input id="delivery_date" name="delivery_date" style="width: 100%; text-align: left"/>
                    <div id="delivery_date_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>

            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">支払方法</div>
                <div class="col-md-10 pl-0"><input id="payment_method" name="payment_method" style="width: 100%; text-align: left"/>
                    <div id="payment_method_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <br/>
            <div><span>銀行口座情報</span></div><br/>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">銀行名</div>
                <div class="col-md-10 pl-0"><input id="bank_name" name="bank_name" style="width: 100%; text-align: left"/>
                    <div id="bank_name_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">支店名</div>
                <div class="col-md-10 pl-0"><input id="branch_name" name="branch_name" style="width: 100%; text-align: left"/>
                    <div id="branch_name_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
                <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">口座種類</div>
                <div class="col-md-10 pl-0"><input id="account_type" name="account_type" style="width: 100%; text-align: left"/>
                    <div id="account_type_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">口座番号</div>
                <div class="col-md-10 pl-0"><input id="account_number" name="account_number" style="width: 100%; text-align: left"/>
                    <div id="account_number_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>
            <div class="col-md-12 row mb-3 pl-0">
                <div class="col-md-2 pl-0">口座名義</div>
                <div class="col-md-10 pl-0"><input id="account_holder" name="account_holder" style="width: 100%; text-align: left"/>
                    <div id="account_holder_error" style="color: red; display: none"><span>please enter your</span></div>
                </div>
            </div>  
        </div>

    </div>

</form>

<div id="third_page_parttwo" class="col-md-12 p-0" style="display:none; font-size: 14px">

    <table style="border-collapse: collapse; width: 100%; table-layout:fixed; border: 1px solid #7d7d7d;">

        <tr><td class="real_table_td">&nbsp;</td></tr>
        <tr>
            <td class="real_table_td">フレームカラー : <span id="framecolor_page3" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">シートカラー : <span id="seatcolor_page3" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">備考 : <span id="eighteen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td"><br/></td>
        </tr>
        <tr>
            <td class="real_table_td">見積有効期限 : <span id="ten" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">納品日 : <span id="eleven" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">支払方法 : <span id="twelve" class=""></span></td>
        </tr>
        
        <tr>
            <td class="real_table_td" style="border-top: 1px solid">銀行口座情報</td>
        </tr>
        <tr>
            <td class="real_table_td">銀行名 : <span id="thirteen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">支店名 : <span id="fourteen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">口座種類 : <span id="fifteen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">口座番号 : <span id="sixteen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td">口座名義 : <span id="seventeen" class=""></span></td>
        </tr>
        <tr>
            <td class="real_table_td" style="word-wrap:break-word; padding-right: 10px; font-size: 12px">
                <p>
                    上記金額に国内外の送料、通関料等の配送費は含まれておりません<br/>
                    配送費の金額は御見積書に記載をさせて頂きます。金額はご注文の量により変わります。<br/>
                    ※クレーン車、フォークリフト等の機械が必要となる場合は別途料金になります。（弊社で手配も可能です。）<br/>
                    また、梱包材の処分はオーナー様にお願いしておりますが配送業者が持ち帰る（有料）ことも可能です。<br/>
                    組立については、ほとんどのマシンは組立済の状態での納品ですが、マシンの大きさによってコンテナに入らない商品、組立が比較的簡単な商品におきましては、未組み立ての状態で送らせて頂きます。<br/>
                    基本的にお客様で組み立てて頂きますが、有料の組み立てサービスもご利用いただけます。<br/>
                    ただし事前の予約が必要になります。<br/>
                    搬入については、基本的に車上渡しになります。<br/>
                    （ビルなどの場合、ビル前の道路や駐車場）<br/>
                    基本的に20フィートのコンテナ車で送らせて頂きますので、搬入時に十分なスペースがあるかどうか搬入経路などをお客様自信で確認して頂く必要がございます。<br/>
                    ※重機物になりますので、搬入の際は十分に注意して頂く必要があります。<br/>
                    ●商品保証関係について<br/>
                    保証サービスの内容はお見積書に記載致します。<br/>
                    保証内容は、商品の部品交換のみとなっておりますので、業者に依頼した際に発生した取付け工賃などは、保証対象外となります。<br/>
                    ●単品でのご購入<br/>
                    弊社は、基本的に新規店舗のオープン、もしくは、拡大などで、多数のマシンをご購入して頂くオーナー様をメインに考えておりますので、単品での御購入のお客様においては、いろいろご不便をおかけする場合があります。<br/>
                    例えば、単品での御購入は他のお客様の御注文に合わせての発送になりますので、納期の期間が遅れてしまう可能性がございます。理由といたしましては、１個のコンテナを貸切って商品を輸送するのですが、<br/>
                    コンテナが２／３以上埋まらないと発送が出来ませんので、他のお客様の御注文とご一緒に発送という形をとっているからです。そのため、他のお客様がいつ頃、注文をして頂くかによって、お客様への納品日が変動してしまう恐れがあります。<br/>
                    また、送料につきましても、単体の商品は割高になります。。<br/>
                    これらの点をご理解いただいたうえでの単品の注文をお考えいただきたいと思います。<br/>
                </p>
            </td>
        </tr>

    </table>
    
</div>

<div id="second_group_btn" class="col-md-12 my-5 pl-0 text-center row" style="display: none">
    <div class="col-md-5 big-screen-butt-left"><button id="second_back_button" class="submit_button" onclick="backFromSecond()" style="font-size: 18px">戻る</button></div>
    <div class="col-md-2" style="min-height: 5px;"></div>
    <div class="col-md-5 big-screen-butt-right"><button id="second_submit_button" class="submit_button" onclick="submitFromSecond()" style="font-size: 18px">入力情報確認</button></div>
</div>

<div id="third_group_btn" class="col-md-12 my-5 pl-0 text-center row" style="display: none">
    <div class="col-md-5 big-screen-butt-left"><button id="second_back_button" class="submit_button" onclick="backFromThird()" style="font-size: 18px">戻る</button></div>
    <div class="col-md-2" style="min-height: 5px;"></div>
    <div class="col-md-5 big-screen-butt-right"><button id="second_submit_button" class="submit_button" onclick="submitFromThird()" style="font-size: 18px">見積書作成</button></div>
</div>

<div id="first_button" class="text-center pb-5 pt-5" style="display: none"><button class="submit_button" onclick="showTableOne(0)" style="font-size: 18px">見積作成</button></div>
@stop

@section('custom_js')
<script type="text/javascript">

    $(".checkbox").change(function() {
        // if(this.checked) {
        //     console.log("checked");
        // }else{
        //     console.log("unchecked");
        // }
        var check_count = $('input[type=checkbox]:checked').length ;
        if(check_count > 0){
            //console.log("vvvv: "+ check_count);
            $('#first_button').show();
        }else{
            $('#first_button').hide();
        }
        //console.log("cccc: "+ check_count);
    });


    var last;
    function expand(catid){
        console.log(catid);

        var selectorId = 'show'+catid;
        var bar = 'catbar'+catid;
        
        //$(".other").hide(500);

        if(last == selectorId){
            $("#"+last).toggle(500);
            //$("#"+selectorId).toggle(500);
        }

        if(last != selectorId){
            if(last != null){
                if($("#"+last).is(":hidden") == false){
                    $("#"+last).toggle(500); 
                }
            }
            $("#"+selectorId).toggle(500);
            last = selectorId;
        }

        $('html, body').animate({
                scrollTop: $("#"+bar).offset().top
            }, 500,function(){
                window.location.href = "#"+bar;
            });
        // if($(".other").show)
        // {
        // }
        // $("button").click(function(){
        // });
    }

    function showTableOne(id){
        

        var myCheckboxes = new Array();
        $('input[type=checkbox]:checked').each(function(a) {
            if($(this).val() != id){
                myCheckboxes.push($(this).val());
                //console.log("aa:"+a);
            }else if($(this).val() == id){
                $(this).val(0);
            }
        });
        var framecolor = $('input[name=frame_color]:checked').val();
        var seatcolor = $('input[name=seat_color]:checked').val();
        console.log("aaa:"+framecolor);

        $("#frameclr").text(framecolor);
        $("#seatclr").text(seatcolor);
        $("#framecolor_page3").text(framecolor);
        $("#seatcolor_page3").text(seatcolor);
        //console.log(myCheckboxes);
        //console.log(id);

        var item_number_obj = {};
        var i;
        for(i = 0 ; i < myCheckboxes.length ; ++i){
            var temp_id = myCheckboxes[i];
            //var temp_item = "item"+temp_id;
            var temp = "eqp_count"+temp_id;
            var count_item = $("#"+temp).text();
            if(id == 0){
                item_number_obj[temp_id] = 1;
                //$('#'+temp_item).val(1);
            }else{
                item_number_obj[temp_id] = count_item;
                //$('#'+temp_item).val(count_item);
            }
        }
        console.log(item_number_obj);

        var ajaxurl = "{{route('show-equipment')}}";
        $.ajax({
            url: ajaxurl,
            type: "GET",
            data: {
                    '_token': "{{ csrf_token() }}",
                    'checked_equipments' : myCheckboxes,
                    'framecolor' : framecolor,
                    'seatcolor' : seatcolor,
                    'item_number_obj' : item_number_obj
            },
            success: function(data){
                //console.log("hh:" + data);
                $data = $(data);
                $('#table_second').html($data);
            },
            complete: function (data) {    
            }
        });

        // for(i = 0 ; i < myCheckboxes.length ; ++i){
        //     var temp_id = myCheckboxes[i];
        //     var temp = "eqp_count"+temp_id;
        //     var t = item_number_obj[temp_id];
        //     $("#"+temp).text(t);
        //     console.log(t);
        // }

        $('#first_page').hide();
        $('#first_button').hide();
        $('#table_second').show();
        $('#second_page').show();
        $('#second_group_btn').show();

        $("#navbar").removeClass("condigo_fondos");
        $("#body").removeClass("condigo_fondos");
        $("#profile-menu").hide();

    }

    function backFromSecond(){
        $('#first_page').show();
        $('#first_button').show();
        $('#second_page').hide();
        $('#second_group_btn').hide();
        $('#table_second').hide();

        $("#navbar").addClass("condigo_fondos");
        $("#body").addClass("condigo_fondos");
        $("#profile-menu").show();
    }

    function backFromThird(){
        $('#second_page').show();
        $('#second_group_btn').show();
        //$('#minus_icon').hide();
        $('.hide_plusminus').show();
        $('.action_delete').show();
        $('#discount_dd').show();
        $('#dis_row').show();

        $('#third_part_top').hide();
        $('#third_page_partone').hide();
        $('#third_page_parttwo').hide();
        $('#third_group_btn').hide();

        $('#one').show();
        $('#two').show();
        $('#three').show();
        $('#four').show();
        $('#five').show();
        $('#six').show();
        $('#seven').show();
        $('#eight').show();
        $('#nine').show();
        $('#ten').show();
        $('#eleven').show();
        $('#twelve').show();
        $('#thirteen').show();
        $('#fourteen').show();
        $('#fifteen').show();
        $('#sixteen').show();
        $('#seventeen').show();
    }

    function submitFromThird(){
        var temp = $('#total_price').text();
        $('#total').val(temp);

        temp = $('#ten_percent_price').text();
        $('#total_vat').val(temp);

        temp = $('#discount_price').text();
        $('#total_discount').val(temp);

        if(confirm("こちらの内容でよろしいですか！")){
            $("#invoice_form").submit();
        }
        var type= '{{ Auth::user()->type }}';
        if(type == '1'){
            setTimeout(() => {window.location.href = "{{ route('mitsumori-list')}}"}, 10000);
        } else {
            setTimeout(() => {window.location.href = "{{ route('mitsumori-list-ind')}}"}, 10000);
        }
    }

    function submitFromSecond(){
        $('#third_part_top').show();
        $('#third_page_partone').show();
        $('#third_page_parttwo').show();
        $('#third_group_btn').show();

        $('#second_page').hide();
        $('#second_group_btn').hide();
        //$('#minus_icon').hide();
        $('.hide_plusminus').hide();
        $('.action_delete').hide();
        $('#discount_dd').hide();

        var selectBox = document.getElementById("discount_box");
        var percent_count = selectBox.options[selectBox.selectedIndex].value;

        if(percent_count == 0){
            $('#dis_row').hide();
        }

        //console.log(one);
        var one = $('#quatation_number').val();
        if(one == '' || one == null){
            $('#one').hide();
        }else{
            $('#one').text(one);
        }

        var two = $('#company_name').val();
        if(two == '' || two == null){
            $('#two').hide();
        }else{
            $('#two').text(two);
        }

        var three = $('#company_address').val();
        if(three == '' || three == null){
            $('#three').hide();
        }else{
            $('#three').text(three);
        }

        var four = $('#phone_number').val();
        if(four == '' || four == null){
            $('#four').hide();
        }else{
            $('#four').text(four);
        }

        var five = $('#com_name').val();
        if(five == '' || five == null){
            $('#five').hide();
        }else{
            $('#five').text(five);
        }

        var six = $('#com_address').val();
        if(six == '' || six == null){
            $('#six').hide();
        }else{
            $('#six').text(six);
        }

        var seven = $('#com_phone_number').val();
        seven = '電話: '+seven
        if(seven == '' || seven == null){
            $('#seven').hide();
        }else{
            $('#seven').text(seven);
        }

        var eight = $('#person_incharge').val();
        eight = eight+'様'
        if(eight == '' || eight == null){
            $('#eight').hide();
        }else{
            $('#eight').text(eight);
        }

        var nine = $('#email').val();
        if(nine == '' || nine == null){
            $('#nine').hide();
        }else{
            $('#nine').text(nine);
        }

        var ten = $('#expiration_date').val();
        if(ten == '' || ten == null){
            $('#ten').hide();
        }else{
            $('#ten').text(ten);
        }

        var eleven = $('#delivery_date').val();
        if(eleven == '' || eleven == null){
            $('#eleven').hide();
        }else{
            $('#eleven').text(eleven);
        }

        var twelve = $('#payment_method').val();
        if(twelve == '' || twelve == null){
            $('#twelve').hide();
        }else{
            $('#twelve').text(twelve);
        }

        var thirteen = $('#bank_name').val();
        if(thirteen == '' || thirteen == null){
            $('#thirteen').hide();
        }else{
            $('#thirteen').text(thirteen);
        }

        var fourteen = $('#branch_name').val();
        if(fourteen == '' || fourteen == null){
            $('#fourteen').hide();
        }else{
            $('#fourteen').text(fourteen);
        }

        var fifteen = $('#account_type').val();
        if(fifteen == '' || fifteen == null){
            $('#fifteen').hide();
        }else{
            $('#fifteen').text(fifteen);
        }

        var sixteen = $('#account_number').val();
        if(sixteen == '' || sixteen == null){
            $('#sixteen').hide();
        }else{
            $('#sixteen').text(sixteen);
        }

        var seventeen = $('#account_holder').val();
        if(seventeen == '' || seventeen == null){
            $('#seventeen').hide();
        }else{
            $('#seventeen').text(seventeen);
        }

        var eighteen = $('#remarks').val();
        if(eighteen == '' || eighteen == null){
            $('#eighteen').hide();
        }else{
            $('#eighteen').text(eighteen);
        }
        
    }

    function minus(price, eq_id){

        var eqp_count_with_id = "eqp_count"+eq_id;
        var eqp_count = $("#"+eqp_count_with_id).text();
        console.log("eqq:"+eqp_count);
        if(eqp_count > 1){

            var sum_total = $("#total_price").text();
            sum_total = parseInt(sum_total);
            sum_total -= price;

            var ten_percent = sum_total + (10*sum_total)/100;

            

            //var eqp_count = $("#"+eqp_count_with_id).text();

            var selectBox = document.getElementById("discount_box");
            var percent_count = selectBox.options[selectBox.selectedIndex].value;

            //var percent_count = $("#percent_value").text();
            percent_count = percent_count.charAt(0);
            var amount = parseFloat(ten_percent);
            res = amount - (percent_count*amount)/100.0;
            res = Math.floor(res);
            $("#discount_price").text(res);

            //console.log("as:");
            //console.log("as:"+ price_with_id);
            eqp_count -= 1;
            var total_price_of_item = price*eqp_count;
            var price_with_id = "price"+eq_id;
            $("#"+price_with_id).text(total_price_of_item);
            
            
            $("#"+eqp_count_with_id).text(eqp_count);

            $("#total_price").text(sum_total);
            $("#ten_percent_price").text(ten_percent);

            var item_count_with_id = "item"+eq_id;
            $('#'+item_count_with_id).val(eqp_count);
        }
        //console.log("minus-- "+eqp_count);
    }

    function plus(price, eq_id){
        var eqp_count_with_id = "eqp_count"+eq_id;
        var eqp_count = $("#"+eqp_count_with_id).text();
        eqp_count = parseInt(eqp_count)+1;
        $("#"+eqp_count_with_id).text(eqp_count);

        var sum_total = $("#total_price").text();
        sum_total = parseInt(sum_total);
        sum_total += price;
        $("#total_price").text(sum_total);

        var ten_percent = sum_total + (10*sum_total)/100;
        $("#ten_percent_price").text(ten_percent);

        var total_price_of_item = parseInt(price)*eqp_count;
        var price_with_id = "price"+eq_id;
        $("#"+price_with_id).text(total_price_of_item);

        var selectBox = document.getElementById("discount_box");
        var percent_count = selectBox.options[selectBox.selectedIndex].value;

        //var percent_count = $("#percent_value").text();
        percent_count = percent_count.charAt(0);
        var amount = parseFloat(ten_percent);
        res = amount - (percent_count*amount)/100.0;
        res = Math.floor(res);
        $("#discount_price").text(res);

        var item_count_with_id = "item"+eq_id;
        $('#'+item_count_with_id).val(eqp_count);

    }

    //$('#discount_box').change(discount);

    function discounts(){

        var selectBox = document.getElementById("discount_box");
        var value = selectBox.options[selectBox.selectedIndex].value;
        console.log("amount: "+value);
        
        if(value > 0){
            var res;
            var discount_price = $("#discount_price").text();
            discount_price = parseInt(discount_price);

            var amount = $("#ten_percent_price").text();
            amount = parseFloat(amount);
            res = amount - (value*amount)/100.0;
            res = Math.floor(res);

            //$("#percent_value").text(value+"%");
            $("#yen_sign").show();
            $("#discount_price").show();
            $("#discount_price").text(res);
        }else if(value == 0){
            // var amount = $("#ten_percent_price").text();
            // res = Math.floor(amount);
            // $("#discount_price").text(res);
            $("#yen_sign").hide();
            $("#discount_price").hide();
        }
        
    }
    // $("select.country").change(function(){
    //         var selectedCountry = $(this).children("option:selected").val();
    //         alert("You have selected the country - " + selectedCountry);
    //     });

    $(document).ready(function(){
        
    });

</script>
<script>
    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    }
</script>
@stop