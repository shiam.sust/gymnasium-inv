<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrameColor extends Model
{
    protected $table = 'colors';
    protected $primaryKey = 'id';

}