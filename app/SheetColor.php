<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SheetColor extends Model
{
    protected $table = 'sheetcolor';
    protected $primaryKey = 'id';

}