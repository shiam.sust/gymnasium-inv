<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Color;
use App\Equip;
use App\Series;
use App\User;
use App\Invoice;
use App\FrameColor;
use App\SheetColor;

use Hash;
use Image;
use Auth;
// use PDFX;
use Mail;
use File;
use Dompdf\Dompdf;

class HomeController extends Controller
{
    public function getin()
    {
        if(isset(Auth::user()->id)){
            $equips = Equip::with('categoryForeign')->with('color1')->with('color2')->with('color3')->with('color4')->get();
            $categories = Category::all();
            $frame_colors = FrameColor::all();
            $sheet_colors = SheetColor::all();
            $data = [
                'equips' => $equips,
                'categories' => $categories,
                'framecolors' => $frame_colors,
                'sheetcolors' => $sheet_colors
            ];
            return view('toppage', $data);
        } else {
            return view('get-in');
        }
    }
    public function loggin(Request $request)
    {
        if (!Auth::attempt($request->only(['name', 'password']) )) {
            return redirect()->back()->with('error', 'Credentials do not match');
        }
        return redirect()->to(route('front-home'));
    }
    public function logout()
    {
    	Auth::logout();
    	return redirect()->to(route('login'));
    }
    public function topPage()
    {
        $equips = Equip::with('categoryForeign')->orderBy('series', 'asc')->with('series_name')->with('color1')->with('color2')->with('color3')->with('color4')->get();
        $categories = Category::all();
        $frame_colors = FrameColor::all();
        $sheet_colors = SheetColor::all();
        //return $sheet_colors;
        $data = [
            'equips' => $equips,
            'categories' => $categories,
            'framecolors' => $frame_colors,
            'sheetcolors' => $sheet_colors
        ];
        return view('toppage', $data);
    }

    public function uploadEquip(Request $request)
    {
        $this->validate($request, [
            'image' => 'dimensions:max_width=2700,max_height=2000'
        ]);
        $cats = Category::all();
        $series = Series::all();
        $colors = Color::all();
        $data = [
            'cats' => $cats,
            'colors' => $colors,
            'series' => $series
        ];
        // if(!empty($request->image)){
        //     return redirect()->back()->with('error_message', '画像サイズを軽くして下さい！');
        // }
        return view('new-equipment', $data);
    }

    public function seriesManagement()
    {
        $all_series = Series::all();
        $data = [
            'all_series' => $all_series
        ];
        return view('all-series', $data);
    }

    public function addSeries(Request $request)
    {
        return $request;
        if($request->edit_id > 0){
            $ser = Series::where('id', $request->edit_id)->first();
            $ser->name = $request->series;
            $ser->save();
        } else {
            $ser = new Series();
            $ser->name = $request->series;
            $ser->save();
        }
        return redirect()->back();
    }

    public function allUser(Request $request)
    {
        $all_user = User::get();
        $data = [
            'users' => $all_user
        ];
        return view('user-all', $data);
    }

    public function uploadEquipAction(Request $request)
    {
        //return $request;
        //dd($request);
        if($request->prev_id > 0){
            $name = '';
            if ($request->hasFile('image')) {
                $extension = $request->image->extension();
                $name = time().rand(1000,9999).'.'.$extension;
                $img = Image::make($request->image);
                $img->save(public_path().'/assets/equips/'.$name);
                // $path = $request->image->storeAs('products', $name);
            }
            $eqp = Equip::find($request->prev_id);
            $eqp->name = $request->title;
            $eqp->model_id = $request->model_id;
            $eqp->category = $request->category;
            $eqp->series = $request->series;
            if($name != '')
                $eqp->image = $name;
            $eqp->length = $request->length;
            $eqp->width = $request->width;
            $eqp->height = $request->height;
            $eqp->weight = $request->weight;
            $eqp->price_main = $request->price;
            $eqp->wholesale_price = $request->price_wholesale;
            $eqp->buying_cost_dollar = $request->price_usd;
            $eqp->buying_cost_yen = $request->price_yen;
            $eqp->available_stock = $request->stock;
            $eqp->color_1 = $request->color1;
            $eqp->color_2 = $request->color2;
            $eqp->color_3 = $request->color3;
            $eqp->comment = $request->comment;
            $eqp->save();
        } else {
            $name = '';
            if ($request->hasFile('image')) {
                $extension = $request->image->extension();
                $name = time().rand(1000,9999).'.'.$extension;
                $img = Image::make($request->image);
                $img->save(public_path().'/assets/equips/'.$name);
                // $path = $request->image->storeAs('products', $name);
            }

            $eqp = new Equip();
            $eqp->name = $request->title;
            $eqp->model_id = $request->model_id;
            $eqp->category = $request->category;
            $eqp->series = $request->series;
            $eqp->image = $name;
            $eqp->length = $request->length;
            $eqp->width = $request->width;
            $eqp->height = $request->height;
            $eqp->weight = $request->weight;
            $eqp->price_main = $request->price;
            $eqp->wholesale_price = $request->price_wholesale;
            $eqp->buying_cost_dollar = $request->price_usd;
            $eqp->buying_cost_yen = $request->price_yen;
            $eqp->available_stock = $request->stock;
            $eqp->color_1 = $request->color1;
            $eqp->color_2 = $request->color2;
            $eqp->color_3 = $request->color3;
            $eqp->comment = $request->comment;

            $eqp->save();
        } 
        return redirect()->route('view-products');
    }

    public function viewProducts(Request $request)
    {
        $series_all = Series::get();
        $series = isset($request->series)?$request->series: '';
        if($series != '' && $series != 100){
            $products = Equip::orderBy('id', 'asc')->with('series_name')->where('series', $series)->get();
            $ser = Series::where('id', $series)->first();
            $title = $ser->name;
        } elseif($series == 10000){
            $products = Equip::orderBy('id', 'desc')->with('series_name')->where('series', '>=', 10)->get();
            $title = 'エアロビ機器';
        } else {
            $products = Equip::orderBy('series', 'asc')->with('series_name')->get();
            $title = '全ての商品';
        }
        
        $data = [
            'products' => $products,
            'title' => $title,
            'series' => $series_all,
            'selected_series' => $series
        ];
        return view('all-products', $data);
    }

    public function editEquip($id)
    {
        // dd($id);
        $cats = Category::all();
        $series = Series::all();
        $colors = Color::all();
        $equip = Equip::find($id);
        $data = [
            'cats' => $cats,
            'colors' => $colors,
            'series' => $series,
            'equip' => $equip
        ];
        return view('new-equipment', $data);
    }

    public function showEquipments(Request $request)
    {
        $items = [];
        $price_sum = 0;
        $eqps = $request->checked_equipments;
        $item_obj = $request->item_number_obj;

        //dd($item_number_obj);
        
        for($i = 0 ; $i < sizeof($eqps) ; ++$i){
            $temp = Equip::where('id', $eqps[$i])->first();
            $price_sum += ($temp->price_main)*$item_obj[$temp->id];
            array_push($items, $temp);
        }
        $ten_percent_total = $price_sum + (10*$price_sum)/100;
        $discount_price = $ten_percent_total - (0*$ten_percent_total)/100;
        $html_table = '<table class="table table-dark" style="background-color: white; color: black; border: 1px solid gray; margin-bottom: 0px !important; width: 100%; min-width: 900px">
        <thead>
          <tr>
            <th scope="col" style="border: none">#</th>
            <th scope="col" style="border: none">品番</th>
            <th scope="col" style="border: none">品名</th>
            <th scope="col" style="border: none">写真</th>
            <th scope="col" style="border: none">仕様</th>
            <th scope="col" style="border: none">数量</th>
            <th scope="col" style="text-align: center; border: none">価格</th>
            <th scope="col" style="border: none"></th>
          </tr>
        </thead>
        <tbody>';
        foreach($items as $k => $data){
            $html_table .= '
            <tr>
                <td>'.($k+1).'</td>
                <td>'.$data->model_id.'</td>
                <td style="width: 250px !important">'.$data->name.'</td>
                <td><img src="'.$request->root().'/assets/equips/'.$data->image.'" style="height: 80px; width: 100px"/></td>
                <td style="min-width: 170px">寸法（mm）:
                    (L)'.$data->length.'*(W)'.$data->width.'*(H)'.$data->height.'<br/>
                    N.W.:'.$data->weight.'kgs
                </td>
                <td style="width: 110px">
                    <span id="minus_icon" class="hide_plusminus" onClick="minus('.$data->price_main.','.$data->id.')">
                        <img src="'.$request->root().'/assets/img/minus.png" title="instagram" style="height: 25px; width: 25px"/>
                    </span>
                    <span id="eqp_count'.$data->id.'" class="mx-2">'.$item_obj[$data->id].'</span>
                    <span id="plus_icon" class="hide_plusminus" onClick="plus('.$data->price_main.','.$data->id.')">
                        <img src="'.$request->root().'/assets/img/plus.png" title="instagram" style="height: 25px; width: 25px"/>
                    </span>
                </td>
                <td class="text-right" style="width: 70px">
                ¥<span id="price'.$data->id.'">'.$data->price_main*$item_obj[$data->id].'</span>
                </td>
                <td>
                    <a class="action_delete" onClick="showTableOne('.$data->id.')">
                        <img src="'.$request->root().'/assets/img/delete.png" title="instagram" style="height: 15px; width: 15px"/>
                    </a>
                </td>
                
            </tr>
            <input type="hidden" name="item'.$data->id.'" value="'.$item_obj[$data->id].'" id="item'.$data->id.'">';
        }
        $html_table .= '<tr>
                            <td colspan="4"></td>
                            <td colspan="2" style="text-align: left; padding-right: 20px">合計価格</td>
                            <td class="text-right">¥<span style="" id="total_price">'.$price_sum.'</span></td>
                            <td></td>
                        </tr>';
        $html_table .= '<tr>
                            <td colspan="4"></td>
                            <td colspan="2" style="text-align: left; padding-right: 20px">税込価格</td>
                            <td class="text-right">¥<span style="" id="ten_percent_price">'.$ten_percent_total.'</span></td>
                            <td></td>
                        </tr>';
        $html_table .= '<tr id="dis_row">
                            <td colspan="4"></td>
                            <td colspan="2" style="text-align: right; padding-right: 20px">
                                <div class="text-left" style=""><span>割引価格
                                    <div id="discount_dd" class="btn-group">
                                        <select id="discount_box" name="discount" class="country" onchange="discounts()">
                                            <option value="0">0%</option>
                                            <option value="5">5%</option>
                                            <option value="10">10%</option>
                                            <option value="15">15%</option>
                                            <option value="20">20%</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </td>
                            <td class="text-right"><span id="yen_sign" style="display:none">¥</span><span id="discount_price" style="display:none">'.$discount_price.'</span></td>
                            <td></td>
                        </tr>';


        $html_table .= '
        <input type="hidden" name="total" value="1" id="total">
        <input type="hidden" name="total_vat" value="1" id="total_vat">
        <input type="hidden" name="total_discount" value="1" id="total_discount">
        ';
        return $html_table;
        
    }

    public function invGen(Request $request)
    {
       
        //return $request;
        $equipments_id = $request->equipments;
        //return $equipments_id;

        $totol_eqps = [];
        $count_items = [];
        for($i = 0 ; $i < sizeof($equipments_id) ; ++$i){
            if($equipments_id[$i] != 0){
                $equips = Equip::where('id', $equipments_id[$i])->first();
                array_push($totol_eqps, $equips);
                $index = "item".$equips->id;
                $count_items[$equips->id] = $request->$index; 
            }   
        }

        //return $totol_eqps;
        $num_of_total_eqps = sizeof($totol_eqps);
        $modulas_total_eqps = $num_of_total_eqps%9;
        $total_minus_modular = $num_of_total_eqps - $modulas_total_eqps;
        $total_eight = $total_minus_modular/9;


        $data = [
            'totol_eqps' => $totol_eqps,
            'count_items' => $count_items,
            'request' => $request,
            'email' => $request->email,
            'num_of_total_eqps' => $num_of_total_eqps,
            'modulas_total_eqps' => $modulas_total_eqps,
            'total_minus_modular' => $total_minus_modular,
            'total_eight' => $total_eight,
        ];
        // return view('pdfinvoice', $data);
        $mail_add = [
            'mail_add' => $request->email
        ];
        // $pdf = PDFX::loadView('pdfinvoice', $data);
        $date = date('Y_m_d-H-i-s');
        $filename = "order_".Auth::user()->id."_".$date;
        $directory_path = 'assets/mitsumori';
        if(!File::exists($directory_path)) {
            File::makeDirectory($directory_path, $mode = 0755, true, true);    
        }

        $invoice = new Invoice();
        $invoice->invoice_name = $filename;
        $invoice->creator_id = Auth::user()->id;
        $invoice->save();

        $template = View('pdfinvoice', $data);
        $template = mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8');
        $dompdf = new dompdf();
        $dompdf->load_html($template, 'UTF-8');
        $dompdf->render();
        file_put_contents($directory_path.'/'.$filename.'.pdf', $dompdf->output());
        


        $templatexy = View('pdfinvoice', $data);
        // $templatexy = mb_convert_encoding($templatexy, 'UTF-8');
        $dompdfxy = new dompdf();
        $dompdfxy->load_html($templatexy, 'UTF-8');
        $dompdfxy->render();
        $dompdfxy->stream($filename);
        // $dompdf->save();
        // $dompdf->save($directory_path.'/'.$filename.'.pdf');
        // $dompdf->stream($filename, array('Attachment'=>0));
        $templatex = View('pdfinvoice', $data);
        $templatex = mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8');
        $dompdfx = new dompdf();
        $dompdfx->load_html($template, 'UTF-8');
        $dompdfx->render();
        $email = $request->email;
        // if($email != '' && $email != null){
        //     Mail::send('nullView', $data, function($message)use($data, $dompdfx, $email) {
        //         $message->to($email)
        //         ->subject("invoice")
        //         ->attachData($dompdfx->output(), "invoice.pdf");
        //         });
        // }
        
       
        // $dompdf->save($directory_path.'/'.$filename.'.pdf');
    }

    public function storeNewUser(Request $request)
    {
        //return $request;

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->user_type;
        $user->password = Hash::make($request->password);
        $user->password_raw = $request->password;
        $user->save();

        return redirect()->back();
    }

    public function deleteUser($id)
    {
        //return $id;

        $user = User::where('id', $id)->first();
        $user->delete();
        return redirect()->back();
    }

    public function mitAdmin()
    {
        $all_mitsumori = Invoice::with('creator')->orderBy('id', 'desc')->get();
        return view('mitsumori-list', ['mitsumori' => $all_mitsumori, 'show' => 1]);
    }
    public function mitUser()
    {
        $all_mitsumori = Invoice::where('creator_id', Auth::user()->id)->orderBy('id', 'desc')->with('creator')->get();
        return view('mitsumori-list', ['mitsumori' => $all_mitsumori, 'show' => 0]);
    }
}
