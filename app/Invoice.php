<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    protected $primaryKey = 'id';

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }
}