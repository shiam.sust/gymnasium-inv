<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equip extends Model
{
    protected $table = 'equips';
    protected $primaryKey = 'id';


    public function series_name()
    {
        return $this->hasOne('App\Series', 'id', 'series');
    }
    public function categoryForeign()
    {
        return $this->hasOne('App\Category', 'id', 'category');
    }
    public function color1()
    {
        return $this->hasOne('App\Color', 'id', 'color_1');
    }
    public function color2()
    {
        return $this->hasOne('App\Color', 'id', 'color_2');
    }
    public function color3()
    {
        return $this->hasOne('App\Color', 'id', 'color_3');
    }
    public function color4()
    {
        return $this->hasOne('App\Color', 'id', 'color_4');
    }
}
