<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/default', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@getin')->name('login');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::post('/loggin', 'HomeController@loggin')->name('loggin');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@topPage')->name('front-home');
    Route::get('/upload-equipment', 'HomeController@uploadEquip')->name('upload-eqip');
    Route::get('/series-management', 'HomeController@seriesManagement')->name('series-management');
    Route::post('/add-series', 'HomeController@addSeries')->name('add-series');
    Route::post('/upload-equipment', 'HomeController@uploadEquipAction')->name('upload-form');

    Route::get('/mitsumori-admin', 'HomeController@mitAdmin')->name('mitsumori-list');
    Route::get('/mitsumori-user', 'HomeController@mitUser')->name('mitsumori-list-ind');
    
    Route::get('/view-all', 'HomeController@viewProducts')->name('view-products');
    Route::get('/user-all', 'HomeController@allUser')->name('user-management');

    Route::post('/invoice-generation', 'HomeController@invGen')->name('generate-invoice');
    Route::post('/new-user', 'HomeController@storeNewUser')->name('post-new-user');
    Route::get('/delete-user/{id}', 'HomeController@deleteUser')->name('delete-user');
    Route::get('/show-equipment', 'HomeController@showEquipments')->name('show-equipment');
    Route::get('/edit-equipment/{id}', 'HomeController@editEquip')->name('edit-equipment');
});